#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 17/10/20
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: Check commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: Get dev and create 8 extended paritions
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


source settings.sh
add_par(){
ext_size=$(( $dev_size/8  ))
local p="+$ext_size"
p+="G"
local var="/dev/$user_sd"
sudo fdisk $var << EOF
n
e
1


w
EOF
$( sudo partprobe /dev/$user_sd )
sleep 4
x=1
while [ $x -le 8 ] 
do
  sudo fdisk $var << EOF
n

$p
w
EOF
$( sudo partprobe /dev/$user_sd )
  let x++
  sleep 4
done
$( sudo partprobe /dev/$user_sd )
makefs $user_sd 
is_mountable $user_sd
}
get_main(){
local user_sd=$1
get_dev_size $user_sd
	read -p "Proceed with adding partitions on $user_sd ? `echo $'\n(y/n)> '` " -n 1 -r
	echo " "
	 if [[ $REPLY =~ ^[Yy]$ ]];then 
		add_par
	 else
		echo "TO_DO"
	fi
}
get_device

