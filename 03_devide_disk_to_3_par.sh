#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 17/10/20
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: Check commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: Devide disk to 3 primary partitions where the 3rd is the largest partition
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


source settings.sh
add_par(){
local p="+$1G"
local var="/dev/$user_sd"
sudo fdisk $var << EOF
n
p


$p
w
EOF
get_df_hT $user_sd
is_mountable $user_sd
}
get_main(){
local user_sd=$1
get_dev_size $user_sd
	read -p "Proceed with adding partitions on $user_sd ? `echo $'\n(y/n)> '` " -n 1 -r
	echo " "
	 if [[ $REPLY =~ ^[Yy]$ ]];then 
		add_par $p1_size
		add_par $p2_size
		add_par $p3_size
	 else
		echo "TO_DO"
	fi
}
get_device

