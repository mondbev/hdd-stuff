#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 17/10/20
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: check commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: Get sd* from user and del part
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source settings.sh
del_par(){
local user_sd=$1
local var="/dev/$user_sd"
sudo fdisk $var << EOF
p
d
p
w
EOF
get_df_hT $user_sd
is_mountable $user_sd
}

get_main(){
local user_sd=$1
	read -p "Proceed with partition delete on $user_sd ? `echo $'\n(y/n)> '` " -n 1 -r
	echo " "
	 if [[ $REPLY =~ ^[Yy]$ ]];then 
		del_par $user_sd
	 else
		echo "TO_DO"
	fi
}
get_device
