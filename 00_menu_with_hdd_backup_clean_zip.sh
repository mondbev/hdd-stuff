#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 20/10/20
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: Check commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: 
#	Display menu to user for:
#	- Backup MBR, /home, /root, /
#		- Zip them up
#	- Clear swap partition

#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


main(){

#define global vars

l="nnnnnnnnnnnnnnnnn" # Decoration
backup_dir="hddStuff" # Program backup folder
root_location=$(df / | grep / | cut -d" " -f1) # Check where OS mounted from
root_dev=${root_location:0:8} # Set root device based on OS mount point
_date=$(date +%Y%m%d_%H-%M-%S_)

get_mounted_devices

show_menu

}

### Get current mounted devices and store in array to use later

get_mounted_devices(){

mounted_dev=$(cat /etc/mtab | grep "/dev/\<sd..\>" | cut -d" " -f1 | cut -d"/" -f3)

# Save input as array -> split new line to array index and restore IFS

SAVEIFS=$IFS  
IFS=$'\n'    
m_dev_arr=($mounted_dev) 
IFS=$SAVEIFS 

}

### Display a select menu to get source and destination

show_menu(){
local src=""

deco_simple "Please select utility"

	select optio in "MBR" "Home" "Root" "Everything" "Clear_swap"; 
	 do
		case $optio in
			MBR) src="MBR"; break;;
			Home) src="Home"; break;;
			Root) src="Root"; break;;
			Everything) src="All"; break;;
			Clear_swap) clear_swap 		
		esac
	 done

deco "Next to destination menu"

# Calculate size if Home or All selected

if [ "$src" = "Home" ] || [ "$src" = "All" ];then

deco_simple "Please sit tight calculating size. it can take a while...."

	if [ "$src" = "Home" ];then
		
		local _size=$(sudo du -s $HOME | awk '{print $1*1000}')

	elif [ "$src" = "All" ];then
		
		local _size=$(sudo du -s / | awk '{print $1*1000}')
	fi
fi

# Use mounted devices array we created earlier to get User Destination

	for dev in "${!m_dev_arr[@]}"; do
		
		if [[ -n $_size ]];then			
			
		  local size_available=$(df -T | grep "${m_dev_arr[$dev]}" | awk '{print $5*1000}')
		  local calc_size=$(( $size_available - $_size  ))
			 
			if [ "$calc_size" -gt "0" ];then

				local _sum=$(numfmt --to iec --format "%0.2f" $calc_size)
	
	    		 	deco_menu "$dev" "${m_dev_arr[$dev]}" "Free space after operation: $_sum"

			 else
				deco_menu "$dev" "${m_dev_arr[$dev]}" "Warning! not enough free space"
			 fi
		else
			deco_menu "$dev" "${m_dev_arr[$dev]}" "^_^"
		fi
	done

deco_simple "Select where to save $src backup from the above list: "
	

# Read user input from device(destination) selection menu

IFS= read -r opt
if [[ $opt =~ ^[0-9]+$ ]] && (( (opt >= 0) && (opt <= "${#m_dev_arr[@]}") ));then
	
	_device="${m_dev_arr[$opt]}"
fi

IFS=$SAVEIFS

# Now find where this device is mounted to

where_is_mounted=$(cat /etc/mtab | grep "/dev/\<$_device\>" | cut -d" " -f2)

if [ "$where_is_mounted" = "/"  ];then

	dst="/$backup_dir"

else

	dst="$where_is_mounted/$backup_dir"

fi
	if [[ ! -d "$dst"  ]];then
		sudo mkdir $dst
	fi

_help "Storing $src backup file in $dst"

_backup "$src" "$dst"

}
_backup(){
local src="$1"
local dst="$2"

if [[ -z $src  ]] || [[ -z $dst  ]];then

_help

else

file_name="$_date-$src-backup.img"

	if [[ -z $root_dev ]];then
		_help
	fi

_help "Starting img creation of $file_name"

	if [ "$src" = "MBR" ];then

		$(sudo dd if=$root_dev of=$dst/$file_name count=2047 bs=1)
		_help "Done creating img of $file_name at $dst"
		exit 0

	elif [ "$src" = "Home" ];then
	
		src="$HOME"
	
	elif [ "$src" = "Root" ];then
	
		src="/root"
	
	elif [ "$src" = "All" ];then 
	
		src="$root_dev"
	fi

sudo find $src -type d -exec mkdir -p "$dst{}" \;
sudo find $src -type f -exec dd if={} of="$dst{}" bs=8M oflag=direct \;

_zip "${src:1}" "$_date-${src:1}-backup" "$dst"

fi

}

_zip(){
local src="$1"
local zip_name="$2"
local dst="$3"

tar_name="$zip_name.tar.gz"

cd $dst

sudo tar --exclude="$dst" -czvf $tar_name $src

_help "All done $zip_name can be found at $dst"
exit 0

}

clear_swap(){
	
	sudo swapoff -a
	
		_help "Cleared swap"
	
	sudo swapon -a
exit 0

}

deco(){

_time=2.5
clear
printf "$l\n# %s\n$l\n" "$@"
sleep $_time
clear

}

deco_menu(){

printf "\n %s) %s (%s)\n" "$@"

}

deco_simple(){

printf "$l\n# %s\n$l\n" "$@"

}

_help(){
if [[ -z $@ ]];then
   deco  "please provide answer"
   exit 1
else
deco "$@"
fi

}

################# Do not remove ################

main
