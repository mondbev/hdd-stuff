#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 17/10/20
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: Check commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: Get dev and add 2 primary partitions of 1GB each
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source settings.sh
add_par(){
local user_sd=$1
local var="/dev/$user_sd"
sudo fdisk $var << EOF
n
p


+1G
w
EOF
get_df_hT $user_sd
is_mountable $user_sd
}
get_main(){
local user_sd=$1
	read -p "Proceed with adding partitions on $user_sd ? `echo $'\n(y/n)> '` " -n 1 -r
	echo " "
	 if [[ $REPLY =~ ^[Yy]$ ]];then 
		add_par $user_sd
		add_par $user_sd
	 else
		echo "TO_DO"
	fi
}
get_device
